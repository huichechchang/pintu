﻿using UnityEngine;
using UnityEngine.UI;

public class GetGameStart : MonoBehaviour
{
    [SerializeField]
    private GameObject currentObject;

    [SerializeField]
    private Button StartBtton;

    public GameObject GetCurrentObject()
    {
        return currentObject;
    }

    public Button GetStartButton()
    {
        return StartBtton;
    }
}