﻿using UnityEngine;
using UnityEngine.UI;

public class GameStartFlow : FlowBase
{
    private GameObject currentMainObject;

    private Button startButton;

    private bool isFinsh = false;
    public override void OnStart()
    {
        var mainObject = GameManager.inatance.GetGameStart();

        currentMainObject = mainObject.GetCurrentObject();

        currentMainObject.SetActive(true);

        startButton = mainObject.GetStartButton();

        startButton.onClick.RemoveAllListeners();

        startButton.onClick.AddListener(() => isFinsh = true);
    }
    public override void OnUpdate()
    {
        base.OnUpdate();
    }
    public override bool IsComplete()
    {
        return isFinsh;
    }
    public override void OnExit()
    {
        currentMainObject.SetActive(false);
    }
}