using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager
{
    public static GameManager inatance;

    private GetGameStart gameStart;

    private GetGameInstructions gameInstructions;

    private GetHowToPlay howToPlay;

    private GetCountDown countDown;

    private GetGameView gameView;

    private GetGameCountDown gameCountDown;

    private GetEndView gameEndView;

    private GetAutoView gameAutoView;

    private GetHomeView gameHomeView;

    private int currentExaminationIndex = 0;

    public enum Gamelevel { Easy, Hard };

    public Gamelevel gamelevel;

    public void Init()
    {
        inatance = this;
    }
    public void SetStartObjects()
    {
        gameStart = GameObject.FindObjectOfType<GetGameStart>(true);
        if (gameStart != null)
        {
            gameStart.GetCurrentObject().SetActive(false);
        }

        gameInstructions = GameObject.FindObjectOfType<GetGameInstructions>(true);
        if (gameInstructions != null)
        {
            gameInstructions.GetCurrentObject().SetActive(false);
        }

        howToPlay = GameObject.FindObjectOfType<GetHowToPlay>(true);
        if (howToPlay != null)
        {
            howToPlay.GetCurrentObject().SetActive(false);
        }

        countDown = GameObject.FindObjectOfType<GetCountDown>(true);
        if (countDown != null)
        {
            countDown.GetCurrentObject().SetActive(false);
        }
    }
    public void SetGameObjects()
    {
        gameCountDown = GameObject.FindObjectOfType<GetGameCountDown>(true);
        if (gameCountDown != null)
        {
            gameCountDown.GetCurrentObject().SetActive(false);
        }

        gameView = GameObject.FindObjectOfType<GetGameView>(true);
        if (gameView != null)
        {
            gameView.GetCurrentObject().SetActive(false);
        }

        gameEndView = GameObject.FindObjectOfType<GetEndView>(true);
        if (gameEndView != null)
        {
            gameEndView.GetCurrentObject().SetActive(false);
        }

        gameAutoView = GameObject.FindObjectOfType<GetAutoView>(true);
        if (gameAutoView != null)
        {
            gameAutoView.GetCurrentObject().SetActive(false);
        }

        gameHomeView = GameObject.FindObjectOfType<GetHomeView>(true);
        if (gameHomeView != null)
        {
            gameHomeView.GetCurrentObject().SetActive(false);
        }

    }


    public void SetCurrentExaminationIndex(int currentExaminationIndex)
    {
        this.currentExaminationIndex = currentExaminationIndex;
    }
    public int GetCurrentExaminationIndex()
    {
        return currentExaminationIndex;
    }

    public GetGameStart GetGameStart()
    {
        if (gameStart != null)
        {
            gameStart.GetCurrentObject().SetActive(false);
            return gameStart;
        }

        Debug.LogError($"gameStart is null,Please Check Prefabs or the Prefabs is in the current Scene");

        return null;
    }
    public GetGameInstructions GetGetGameInstructions()
    {
        if (gameInstructions != null)
        {
            gameInstructions.GetCurrentObject().SetActive(false);
            return gameInstructions;
        }

        Debug.LogError($"gameInstructions is null,Please Check Prefabs or the Prefabs is in the current Scene");

        return null;
    }
    public GetHowToPlay GetHowToPlay()
    {
        if (howToPlay != null)
        {
            howToPlay.GetCurrentObject().SetActive(false);
            return howToPlay;
        }

        Debug.LogError($"howToPlay is null,Please Check Prefabs or the Prefabs is in the current Scene");

        return null;
    }

    public GetCountDown GetCountDown()
    {
        if (countDown != null)
        {
            countDown.GetCurrentObject().SetActive(false);
            return countDown;
        }

        Debug.LogError($"countDown is null,Please Check Prefabs or the Prefabs is in the current Scene");

        return null;
    }

    public GetGameCountDown GetGameCountDown()
    {
        if (gameCountDown != null)
        {
            gameCountDown.GetCurrentObject().SetActive(false);
            return gameCountDown;
        }

        Debug.LogError($"gameCountDown is null,Please Check Prefabs or the Prefabs is in the current Scene");

        return null;
    }
    public GetGameView GetGameView()
    {
        if (gameView != null)
        {
            gameView.GetCurrentObject().SetActive(false);
            return gameView;
        }

        Debug.LogError($"gameView is null,Please Check Prefabs or the Prefabs is in the current Scene");

        return null;
    }
    public GetEndView GetEndView()
    {
        if (gameEndView != null)
        {
            gameEndView.GetCurrentObject().SetActive(false);
            return gameEndView;
        }

        Debug.LogError($"gameEndView is null,Please Check Prefabs or the Prefabs is in the current Scene");

        return null;
    }

    public GetAutoView GetAutoView()
    {
        if (gameAutoView != null)
        {
            gameAutoView.GetCurrentObject().SetActive(false);
            return gameAutoView;
        }

        Debug.LogError($"gameAutoView is null,Please Check Prefabs or the Prefabs is in the current Scene");

        return null;
    }

    public GetHomeView GetHomeView()
    {
        if (gameHomeView != null)
        {
            gameHomeView.GetCurrentObject().SetActive(false);
            return gameHomeView;
        }

        Debug.LogError($"gameHomeView is null,Please Check Prefabs or the Prefabs is in the current Scene");

        return null;
    }

    public Gamelevel GetGamelevel()
    {
        return gamelevel;
    }

    public void SetGameLevel(Gamelevel gamelevel)
    {
        this.gamelevel = gamelevel;
    }
}
