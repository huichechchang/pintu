using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragHandlerUI : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private Vector3 originalPos;
    private Vector2 mousePos;
    private RectTransform canvasRec;

    public delegate void OnCheckBlock(GameObject thisObject, GameObject CheckedObject);
    public OnCheckBlock onCheck;

    void Start()
    {
        canvasRec = this.GetComponentInParent<Canvas>().transform as RectTransform;
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        originalPos = this.GetComponent<RectTransform>().anchoredPosition;

        RectTransformUtility.ScreenPointToLocalPointInRectangle(canvasRec, eventData.position, eventData.pressEventCamera, out mousePos);
    }

    public void OnDrag(PointerEventData eventData)
    {
        this.GetComponent<Image>().raycastTarget = false;

        Vector2 newCanvasRec;

        RectTransformUtility.ScreenPointToLocalPointInRectangle(canvasRec, eventData.position, eventData.pressEventCamera, out newCanvasRec);

        Vector3 offest = new Vector3(newCanvasRec.x - mousePos.x, newCanvasRec.y - mousePos.y, 0);

        (this.transform as RectTransform).anchoredPosition = originalPos + offest;

    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("�˴�");
        var currentCheckObjec = eventData.pointerCurrentRaycast.gameObject;
        if (currentCheckObjec != null)
        {
            Debug.Log(currentCheckObjec.name);
            onCheck?.Invoke(this.gameObject, currentCheckObjec);
        }

        this.GetComponent<Image>().raycastTarget = true;
    }
}
