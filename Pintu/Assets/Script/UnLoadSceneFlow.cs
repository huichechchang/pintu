﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class UnLoadSceneFlow : FlowBase
{
    private string sceneName;
    private AsyncOperation asyncOperation;
    private Action action;
    public UnLoadSceneFlow(string sceneName, Action action)
    {
        this.sceneName = sceneName;
        this.action = action;
    }
    public override void OnStart()
    {
        asyncOperation = SceneManager.UnloadSceneAsync(sceneName, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
        asyncOperation.allowSceneActivation = false;
    }
    public override void OnUpdate()
    {
        if (asyncOperation.isDone)
        {
            action?.Invoke();

            asyncOperation.allowSceneActivation = true;

            Debug.Log($"場景卸載完畢{asyncOperation.allowSceneActivation}");
        }
    }
    public override bool IsComplete()
    {
        return asyncOperation.allowSceneActivation;
    }
}
