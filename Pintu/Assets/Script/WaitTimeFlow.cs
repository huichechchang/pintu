﻿using UnityEngine;

public class WaitTimeFlow : FlowBase
{
    private float timer;

    public WaitTimeFlow(float timer)
    {
        this.timer = timer;
    }
    public override void OnStart()
    {
        base.OnStart();
    }
    public override void OnUpdate()
    {
        timer -= Time.deltaTime;
    }
    public override bool IsComplete()
    {
        return timer <= 0;
    }
}