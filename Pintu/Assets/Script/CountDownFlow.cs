using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class CountDownFlow : FlowBase
{
    private GameObject currentMainObject;
    private Image countDownImage;
    private List<Sprite> countDownSprites = new List<Sprite>();
    private int countdownTimes = 0;


    private bool isFinsh = false;
    public override async void OnStart()
    {
        var mainObject = GameManager.inatance.GetCountDown();

        currentMainObject = mainObject.GetCurrentObject();

        currentMainObject.SetActive(true);

        countDownImage = mainObject.GetCountDownImage();

        countDownSprites = mainObject.GetCountDownSprites();

        countdownTimes = countDownSprites.Count - 1;

        countDownImage.sprite = countDownSprites[countdownTimes];


        await StartCountdownAsync();
    }
    async Task StartCountdownAsync()
    {
        float currentTime = countdownTimes;

        while (currentTime > 0)
        {
            UpdateCountDownSprite(currentTime);
            await Task.Delay(1000);
            currentTime--;
        }

        isFinsh = true;
    }
    private void UpdateCountDownSprite(float currentTime)
    {
        var Index = (int)currentTime;
        countDownImage.sprite = countDownSprites[Index];
        countDownImage.SetNativeSize();
    }
    public override void OnUpdate()
    {
    }
    public override bool IsComplete()
    {
        return isFinsh;
    }
}
