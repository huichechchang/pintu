﻿using UnityEngine;
using UnityEngine.UI;

public class EndGameFlow : FlowBase
{
    private GameObject currentMainObject;
    private Image animalFinshImage;
    private Examination examination;

    private bool isFinsh = false;

    public EndGameFlow(Examination examination)
    {
        this.examination = examination;
    }
    public override void OnStart()
    {
        var mainObject = GameManager.inatance.GetEndView();

        currentMainObject = mainObject.GetCurrentObject();

        animalFinshImage = mainObject.GetAnimalFinshImage();

        var examinationIndex = GameManager.inatance.GetCurrentExaminationIndex();

        switch (GameManager.inatance.gamelevel)
        {
            case GameManager.Gamelevel.Easy:
                animalFinshImage.sprite = examination.easyexaminationBase[examinationIndex].finshSprite;
                break;
            case GameManager.Gamelevel.Hard:
                animalFinshImage.sprite = examination.hardexaminationBase[examinationIndex].finshSprite;
                break;
        }

        animalFinshImage.SetNativeSize();
        animalFinshImage.gameObject.SetActive(true);

        currentMainObject.SetActive(true);

        isFinsh = true;
    }
    public override void OnUpdate()
    {
        base.OnUpdate();
    }
    public override bool IsComplete()
    {
        return isFinsh;
    }
}
