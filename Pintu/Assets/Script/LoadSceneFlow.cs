using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class LoadSceneFlow : FlowBase
{
    private string sceneName;
    private AsyncOperation asyncOperation;
    private Action action;
    private LoadSceneMode loadScenemode;
    private bool isFinsh = false;
    public LoadSceneFlow(string sceneName, Action action, LoadSceneMode loadSceneMode)
    {
        this.sceneName = sceneName;
        this.action = action;
        this.loadScenemode = loadSceneMode;
    }
    public override void OnStart()
    {
        asyncOperation = SceneManager.LoadSceneAsync(sceneName, loadScenemode);
    }
    public override void OnUpdate()
    {
        if (asyncOperation.isDone)
        {
            Debug.Log($"����Ū������{asyncOperation.allowSceneActivation}");

            action?.Invoke();

            isFinsh = true;
        }
    }
    public override bool IsComplete()
    {
        return isFinsh;
    }
}
