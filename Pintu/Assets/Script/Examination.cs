using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New ExaminationData", menuName = "Custom/ExamationData")]
public partial class Examination : ScriptableObject
{
    public List<ExaminationBase> easyexaminationBase;

    public List<ExaminationBase> hardexaminationBase;

    [Serializable]
    public struct ExaminationBase
    {
        public string topic;

        public GameObject currentExaminationObject;

        public Sprite topicSprite;

        public Sprite gridSprite;

        public Sprite finshSprite;

        public List<Block> buildingBlock;
    }
    [Serializable]
    public struct Block
    {
        public Sprite originalBlock;

        public Sprite correctBlock;
    }
}
