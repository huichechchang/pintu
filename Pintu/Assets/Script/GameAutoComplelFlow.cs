﻿using UnityEngine;

public class GameAutoComplelFlow : FlowBase
{
    private GameObject gameAutoCompleteViewObj;
    private bool isFinsh;
    public override void OnStart()
    {
        var gameAutoCompleteView = GameManager.inatance.GetAutoView();
        gameAutoCompleteViewObj = gameAutoCompleteView.GetCurrentObject();
        gameAutoCompleteViewObj.SetActive(true);

        isFinsh = true;
    }
    public override bool IsComplete()
    {
        return isFinsh;
    }
}