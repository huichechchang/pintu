using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneName
{
    public const string Start = nameof(Start);

    public const string GameMain = nameof(GameMain);
}
