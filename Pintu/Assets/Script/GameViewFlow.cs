﻿public class GameViewFlow : FlowBase
{
    private Sqeuare mainFlow = new Sqeuare();
    private Examination examination;
    private bool isFinsh = false;

    public GameViewFlow(Examination examination)
    {
        this.examination = examination;
    }
    public override void OnStart()
    {
        SetHomeView();

        GameMain gameMain = new GameMain();
        gameMain.SetExaminationData(examination);

        mainFlow.AddFlow(gameMain);
        mainFlow.AddFlow(new EndGameFlow(examination));
        mainFlow.AddFlow(new WaitTimeFlow(5f));

        mainFlow.OnStart();
    }
    private void SetHomeView()
    {
        var homeView = GameManager.inatance.GetHomeView();

        homeView.GetCurrentObject().SetActive(true);

        homeView.GetHomeButton().onClick.AddListener(() => isFinsh = true);
    }
    public override void OnUpdate()
    {
        mainFlow.OnUpdate();
        if (mainFlow.IsComplete())
        {
            isFinsh = true;
        }
    }
    public override bool IsComplete()
    {
        return isFinsh;
    }
    public override void OnExit()
    {
        mainFlow.ClearFlow();
    }
}