using System.Collections;
using System.Collections.Generic;

public class GameMain : FlowBase
{
    private Partion mainFlow = new Partion();
    private Examination examinationData;

    public void SetExaminationData(Examination examination)
    {
        this.examinationData = examination;
    }
    public override void OnStart()
    {

        var gameCountDownMainFlow = new Sqeuare();
        var gameCountDownFlow = new GameCountDownFlow();

        gameCountDownMainFlow.AddFlow(gameCountDownFlow);
        gameCountDownMainFlow.AddFlow(new GameAutoComplelFlow());
        gameCountDownMainFlow.AddFlow(new WaitTimeFlow(3.0f));

        var gameFlow = new GameFlow();
        gameFlow.SetExaminationData(examinationData);
        gameFlow.SetCountDownFlow(gameCountDownFlow);

        mainFlow.AddFlow(gameFlow);
        mainFlow.AddFlow(gameCountDownMainFlow);

        mainFlow.OnStart();
    }
    public override void OnUpdate()
    {
        mainFlow.OnUpdate();
    }

    public override bool IsComplete()
    {
        return mainFlow.IsComplete();
    }
}
