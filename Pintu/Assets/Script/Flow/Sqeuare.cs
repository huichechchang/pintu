using System.Collections.Generic;
using UnityEngine;

public class Sqeuare : FlowBase
{
    private List<FlowBase> flows = new List<FlowBase>();
    private FlowBase currentFlow = null;
    private int currentIndex = 0;
    public void AddFlow(FlowBase flow)
    {
        flows.Add(flow);
    }
    public void RemoveFlow(FlowBase flow)
    {
        flows.Remove(flow);
    }

    public override void OnStart()
    {
        base.OnStart();

        currentIndex = 0;
        currentFlow = null;
        StartFlow();
    }
    private void StartFlow()
    {
        if (currentIndex < flows.Count)
        {
            currentFlow = flows[currentIndex];
            currentFlow.OnStart();
        }
        else
        {
            currentFlow = null;
        }
    }

    public override void OnUpdate()
    {
        if (currentFlow != null)
        {
            currentFlow.OnUpdate();
            if (currentFlow.IsComplete())
            {
                NextFlow();
            }
        }
    }
    private void NextFlow()
    {
        if (currentFlow != null)
        {
            currentFlow.OnExit();
        }
        currentIndex += 1;
        StartFlow();
    }
    public void PrevFlow()
    {
        if (currentFlow != null)
        {
            currentFlow.OnExit();
        }
        currentIndex -= 1;
        currentIndex = Mathf.Max(currentIndex, 0);
        StartFlow();
    }
    public override void OnExit()
    {
        foreach (FlowBase flow in flows)
        {
            flow.OnExit();
        }
    }
    public override bool IsComplete()
    {
        return currentIndex >= flows.Count;
    }

    public void ClearFlow()
    {
        flows.Clear();
    }
}
