
public abstract class FlowBase
{
    public virtual void OnStart()
    {

    }

    public virtual void OnUpdate()
    {

    }
    public abstract bool IsComplete();

    public virtual void OnExit()
    {

    }

}
