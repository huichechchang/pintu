﻿using System.Collections.Generic;

public class Partion : FlowBase
{
    protected List<FlowBase> mainFlow = new List<FlowBase>();

    public void AddFlow(FlowBase flow)
    {
        mainFlow.Add(flow);
    }
    public void RemoveFlow(FlowBase flow)
    {
        mainFlow.Remove(flow);
    }
    public void ClearFlow(FlowBase flow)
    {
        mainFlow.Clear();
    }

    public override void OnStart()
    {
        foreach (FlowBase flow in mainFlow)
        {
            flow.OnStart();
        }
    }

    public override void OnUpdate()
    {
        foreach (FlowBase flow in mainFlow)
        {
            flow.OnUpdate();
        }
    }
    public override void OnExit()
    {
        foreach (FlowBase flow in mainFlow)
        {
            flow.OnExit();
        }
    }
    public override bool IsComplete()
    {
        foreach (FlowBase flow in mainFlow)
        {
            if (flow.IsComplete() == true)
            {
                return true;
            }
        }
        return false;
    }
}
