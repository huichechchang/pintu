﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class GameFlow : FlowBase
{
    private DragHandlerUI[] dragHandlerUIs;
    private Examination examinationData;
    private GameCountDownFlow gameCountDownFlow;

    private GameObject gameViewObject;
    private Image topic;
    private Image gridImage;

    private int answerCount;
    private int questionCount;

    private bool isFinsh = false;

    public void SetExaminationData(Examination examination)
    {
        this.examinationData = examination;
    }
    public void SetCountDownFlow(GameCountDownFlow gameCountDownFlow)
    {
        this.gameCountDownFlow = gameCountDownFlow;
    }
    public override void OnStart()
    {
        answerCount = 0;

        SetExamination();

        AddBlockListener();

        gameCountDownFlow.onCompleteCallback += ShowGrid;
    }

    private void ShowGrid()
    {
        gridImage.gameObject.SetActive(true);
    }

    public void AddBlockListener()
    {
        dragHandlerUIs = GameObject.FindObjectsByType<DragHandlerUI>(FindObjectsInactive.Include, FindObjectsSortMode.None);
        foreach (var dragHandler in dragHandlerUIs)
        {
            dragHandler.GetComponent<Image>().alphaHitTestMinimumThreshold = 0.5f;
            dragHandler.onCheck += Check;
        }
    }//添加拼圖監聽
    public void SetExamination()
    {
        var currentExaminationIndex = 0;

        var gameView = GameManager.inatance.GetGameView();

        topic = gameView.GetExaminationTopicImage();

        gridImage = gameView.GetExaminationGrid();

        switch (GameManager.inatance.GetGamelevel())
        {
            case GameManager.Gamelevel.Easy:

                currentExaminationIndex = UnityEngine.Random.Range(0, examinationData.easyexaminationBase.Count);

                topic.sprite = examinationData.easyexaminationBase[currentExaminationIndex].topicSprite;
                topic.SetNativeSize();

                gridImage.sprite = examinationData.easyexaminationBase[currentExaminationIndex].gridSprite;
                gridImage.SetNativeSize();
                gridImage.gameObject.SetActive(false);

                var easyexamination = examinationData.easyexaminationBase[currentExaminationIndex].currentExaminationObject;

                GameObject.Instantiate(easyexamination, topic.transform);

                break;

            case GameManager.Gamelevel.Hard:

                currentExaminationIndex = UnityEngine.Random.Range(0, examinationData.hardexaminationBase.Count);

                topic.sprite = examinationData.hardexaminationBase[currentExaminationIndex].topicSprite;
                topic.SetNativeSize();

                gridImage.sprite = examinationData.hardexaminationBase[currentExaminationIndex].gridSprite;
                gridImage.SetNativeSize();
                gridImage.gameObject.SetActive(false);

                var hardexamination = examinationData.hardexaminationBase[currentExaminationIndex].currentExaminationObject;

                GameObject.Instantiate(hardexamination, topic.transform);

                break;
        }

        questionCount = examinationData.easyexaminationBase[currentExaminationIndex].buildingBlock.Count;

        GameManager.inatance.SetCurrentExaminationIndex(currentExaminationIndex);

        gameViewObject = gameView.GetCurrentObject();
        gameViewObject.SetActive(true);
    }//設定題目
    public void Check(GameObject currentobj, GameObject checkedObject)
    {
        Debug.Log("Check");
        if (currentobj.name == checkedObject.name)
        {
            GameObject.Destroy(currentobj);

            var checkedObjectImage = checkedObject.GetComponent<Image>();
            checkedObjectImage.color = Color.white;
            checkedObjectImage.raycastTarget = false;
            answerCount++;
        }
        if (answerCount >= questionCount)
        {
            isFinsh = true;
        }
    }//檢查是否正確
    public override bool IsComplete()
    {
        return isFinsh;
    }
}
