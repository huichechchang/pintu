using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Main : MonoBehaviour
{
    [SerializeField]
    private Examination examination;

    private GameManager gameManager = new GameManager();

    private Sqeuare mainFlow = new Sqeuare();

    private bool isReset = false;

    void Start()
    {
        gameManager.Init();

        mainFlow.AddFlow(new LoadSceneFlow(SceneName.Start, OnStartLoadDone, LoadSceneMode.Additive));

        Init();
    }
    void Update()
    {
        mainFlow.OnUpdate();
        if (mainFlow.IsComplete() && !isReset)
        {
            isReset = true;
            ResetGame();
        }
    }

    private void Init()
    {
        mainFlow.AddFlow(new GameStartFlow());

        mainFlow.AddFlow(new GameInstructionsFlow());

        mainFlow.AddFlow(new HowToPlayFlow());

        mainFlow.AddFlow(new CountDownFlow());

        mainFlow.AddFlow(new LoadSceneFlow(SceneName.GameMain, OnGameMainLoadDone, LoadSceneMode.Additive));

        mainFlow.AddFlow(new GameViewFlow(examination));

        mainFlow.AddFlow(new LoadSceneFlow(SceneName.Start, ReStart, LoadSceneMode.Additive));

        mainFlow.OnStart();

        isReset = false;
    }
    private void ReStart()
    {
        SceneManager.UnloadSceneAsync(SceneName.GameMain);
        gameManager.SetStartObjects();
    }
    private void OnStartLoadDone()
    {
        gameManager.SetStartObjects();
    }
    private void OnGameMainLoadDone()
    {
        SceneManager.UnloadSceneAsync(SceneName.Start);
        gameManager.SetGameObjects();
    }
    private void ResetGame()
    {
        mainFlow.ClearFlow();
        Init();
    }
}
