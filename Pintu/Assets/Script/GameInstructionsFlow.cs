using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameInstructionsFlow : FlowBase
{
    private GameObject currentMainObject;

    private Button toNextButton;

    private bool isFinsh = false;
    public override void OnStart()
    {
        var mainObject = GameManager.inatance.GetGetGameInstructions();

        currentMainObject = mainObject.GetCurrentObject();

        currentMainObject.SetActive(true);

        toNextButton = mainObject.GetHowToPlayButton();

        toNextButton.onClick.RemoveAllListeners();

        toNextButton.onClick.AddListener(() => isFinsh = true);
    }
    public override void OnUpdate()
    {
        base.OnUpdate();
    }
    public override bool IsComplete()
    {
        return isFinsh;
    }
    public override void OnExit()
    {
        currentMainObject.SetActive(false);
    }
}
