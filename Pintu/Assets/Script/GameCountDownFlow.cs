﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class GameCountDownFlow : FlowBase
{
    private Image countDownBar;
    private Text countDownText;

    private float countDownTime = 45f;
    private float countDownTimer = 45f;

    private bool isShowGrid = false;
    private bool isFinsh = false;

    public delegate void OnCompleteCallback();
    public OnCompleteCallback onCompleteCallback;
    public override void OnStart()
    {
        SetGameCountDown();
    }
    private void SetGameCountDown()
    {
        var gameCountDown = GameManager.inatance.GetGameCountDown();

        gameCountDown.GetCurrentObject().SetActive(true);

        countDownBar = gameCountDown.GetCountDownBar();

        countDownText = gameCountDown.GetCountDownText();
    }
    public override void OnUpdate()
    {
        countDownTimer -= Time.deltaTime;

        if(countDownTimer <= 0)
        {
            isFinsh = true;
        }
        if (countDownTimer > 0)
        {
            SetUpdateGameCountDownBar(countDownTime);
            SetUpdateGameCountDownText(countDownTimer);
        }
        //當時間少於15秒時，出現隔線
        if (countDownTimer < 15 && !isShowGrid)
        {
            isShowGrid = true;
            onCompleteCallback();
        }
    }
    private void SetUpdateGameCountDownBar(float currentTime)
    {
        countDownBar.fillAmount = Map(1, 0f, countDownTime, 0f, countDownTimer);
    }
    private void SetUpdateGameCountDownText(float currentTime)
    {
        countDownText.text = ((int)currentTime).ToString();
    }
    private float Map(float outA, float outB, float inputA, float inputB, float currentValue)
    {
        float outValue = Mathf.InverseLerp(inputA, inputB, currentValue);
        return Mathf.Lerp(outA, outB, outValue);
    }
    public override bool IsComplete()
    {
        return isFinsh;
    }
}