﻿using UnityEngine;
using UnityEngine.UI;

public class HowToPlayFlow : FlowBase
{
    private GameObject currentMainObject;

    private Button easyButton;

    private Button hardButton;

    private bool isFinsh = false;
    public override void OnStart()
    {
        var mainObject = GameManager.inatance.GetHowToPlay(); ;

        currentMainObject = mainObject.GetCurrentObject();

        currentMainObject.SetActive(true);

        easyButton = mainObject.GetEasyButton();

        easyButton.onClick.RemoveAllListeners();

        easyButton.onClick.AddListener(SetEasyLevel);


        hardButton = mainObject.GetHardButton();

        hardButton.onClick.RemoveAllListeners();

        hardButton.onClick.AddListener(SetHardLevel);
    }
    private void SetEasyLevel()
    {
        GameManager.inatance.SetGameLevel(GameManager.Gamelevel.Easy);
        isFinsh = true;
    }

    private void SetHardLevel()
    {
        GameManager.inatance.SetGameLevel(GameManager.Gamelevel.Hard);
        isFinsh = true;
    }
    public override void OnUpdate()
    {
        base.OnUpdate();
    }
    public override bool IsComplete()
    {
        return isFinsh;
    }
    public override void OnExit()
    {
        currentMainObject.SetActive(false);
    }
}
