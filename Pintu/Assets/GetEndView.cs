﻿using UnityEngine;
using UnityEngine.UI;

public class GetEndView : MonoBehaviour
{
    [SerializeField]
    private GameObject currentObject;

    [SerializeField]
    private Image animalFinshImage;

    public GameObject GetCurrentObject()
    {
        return currentObject;
    }

    public Image GetAnimalFinshImage()
    {
        return animalFinshImage;
    }
}
