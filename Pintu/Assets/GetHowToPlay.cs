using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetHowToPlay : MonoBehaviour
{
    [SerializeField]
    private GameObject currentObject;

    [SerializeField]
    private Button easyButton;

    [SerializeField]
    private Button HardButton;

    public GameObject GetCurrentObject()
    {
        return currentObject;
    }

    public Button GetEasyButton()
    {
        return easyButton;
    }

    public Button GetHardButton()
    {
        return HardButton;
    }
}
