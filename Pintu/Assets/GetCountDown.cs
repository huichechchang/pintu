﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetCountDown : MonoBehaviour
{
    [SerializeField]
    private GameObject currentObject;

    [SerializeField]
    private Image countDownImage;

    [SerializeField]
    private List<Sprite> countDownSprites;

    public GameObject GetCurrentObject()
    {
        return currentObject;
    }

    public Image GetCountDownImage()
    {
        return countDownImage;
    }

    public List<Sprite> GetCountDownSprites()
    {
        return countDownSprites;
    }
}

