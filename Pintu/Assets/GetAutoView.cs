using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetAutoView : MonoBehaviour
{
    [SerializeField]
    private GameObject currentObject;

    public GameObject GetCurrentObject()
    {
        return currentObject;
    }
}
