using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetHomeView : MonoBehaviour
{
    [SerializeField]
    private GameObject currentObject;

    [SerializeField]
    private Button homeButton;

    public GameObject GetCurrentObject()
    {
        return currentObject;
    }

    public Button GetHomeButton()
    {
        return homeButton;
    }
}
