﻿using UnityEngine;
using UnityEngine.UI;

public class GetGameInstructions : MonoBehaviour
{
    [SerializeField]
    private GameObject currentObject;

    [SerializeField]
    private Button howToPlayButton;

    public GameObject GetCurrentObject()
    {
        return currentObject;
    }

    public Button GetHowToPlayButton()
    {
        return howToPlayButton;
    }
}

