﻿using UnityEngine;
using UnityEngine.UI;

public class GetGameCountDown : MonoBehaviour
{
    [SerializeField]
    private GameObject currentObject;

    [SerializeField]
    private Image bar;

    [SerializeField]
    private Text countDownText;

    public GameObject GetCurrentObject()
    {
        return currentObject;
    }

    public Image GetCountDownBar()
    {
        return bar;
    }

    public Text GetCountDownText()
    {
        return countDownText;
    }
}