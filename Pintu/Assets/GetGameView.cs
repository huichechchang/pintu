using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetGameView : MonoBehaviour
{
    [SerializeField]
    private GameObject currentObject;

    [SerializeField]
    private Image examinationTopic;

    [SerializeField]
    private Image examinationGrid;

    [SerializeField]
    private List<Image> blockImages;

    public GameObject GetCurrentObject()
    {
        return currentObject;
    }

    public Image GetExaminationTopicImage()
    {
        return examinationTopic;
    }

    public Image GetExaminationGrid()
    {
        return examinationGrid;
    }

    public List<Image> GetBlockImages()
    {
        return blockImages;
    }
}
